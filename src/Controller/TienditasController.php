<?php
/**
 * @file
 * Contains \Drupal\tiendita_onboarding\Controller\TienditasController.
 */

namespace Drupal\tiendita_onboarding\Controller;



class TienditasController {
  
  public static function createTiendita($uid, $name, $alias, $mail, $address, $description, $logo = null, $currency = 'USD', $country = 'PA', $type = 'tiendita') {

    // If needed, this will import the currency.
    $currency_importer = \Drupal::service('commerce_price.currency_importer');
    $currency_importer->import($currency);

    $store = \Drupal\commerce_store\Entity\Store::create([
    'type' => $type,
    'uid' => $uid,
    'name' => $name,
    'mail' => $mail,
    'address' => $address,
    'public_address' => $address,
    'default_currency' => $currency,
    'billing_countries' => [
        $country,
    ],
    'field_logo' => $logo,
    'field_description' => $description,
    'prices_include_tax' => true,
    ]);

      $store->path = [
          'alias' => '/'.$alias,
          'pathauto' => (\Drupal::moduleHandler()->moduleExists('pathauto'))?Drupal\pathauto\PathautoState::SKIP:null,
      ];

    // Finally, save the store.
    $store->save();
    return $store;
  }
  
  public static function assignTienditaPaymentGateways($sid,$payment_methods){
    foreach($payment_methods as $method){
      $payment_gateway = \Drupal\commerce_payment\Entity\PaymentGateway::load($method);
      //$payment_conditions = $payment_gateway->getPlugin()->getConditions();

      $payment_gateway->set('conditions',[[
        'plugin' => 'order_store',
        'configuration' => [
          'stores' => [$sid],
        ],
      ]]);
      $payment_gateway->save();
    }
  }

}