<?php
namespace Drupal\tiendita_onboarding\Plugin\WebformHandler;
 
 use Drupal\Core\Form\FormStateInterface;
 use Drupal\webform\Plugin\WebformHandlerBase;
 use Drupal\webform\WebformSubmissionInterface;
  
 /**
  * Form submission handler.
  *
  * Redirects to the [...] after the submit.
  *
  * @WebformHandler(
  *   id = "tiendita_onboarding_create_tiendita",
  *   label = @Translation("Create User's First Tiendita"),
  *   category = @Translation("Webform Handler"),
  *   description = @Translation("Create User's First Tiendita"),
  *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
  *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
  * )
  */
 class CreateTienditaWebformHandler extends WebformHandlerBase {
  
   public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
  
     $values = $webform_submission->getData();

     if (!empty($values['nombre_de_la_tiendita'])) {
       $uid = \Drupal::currentUser()->id();
       $name = $values['nombre_de_la_tiendita'];
       $alias = $values['tiendita_urlname'];
       $mail = $values['tiendita_contact']['email'];
       if(!empty($values['tiendita_contact']['name'])){
         $names = explode(" ",$values['tiendita_contact']['name'], 2);
         $first_name = $names [0];
         if(!empty($names[1]))
           $last_name = $names[1];
       }
       $address = array(
        'given_name' => $first_name,
        'family_name' => $last_name,
        'country_code' => 'PA',
        'address_line1' => $values['tiendita_contact']['address'],
        'address_line2' => $values['tiendita_contact']['address_2'],
        'locality' => $values['tiendita_contact']['city'],
        'administrative_area' => $values['tiendita_contact']['state_province']
       );
       $logo = $values['logo'];
       $description = $values['tiendita_description'];

      $tiendita = \Drupal\tiendita_onboarding\Controller\TienditasController::createTiendita($uid, $name, $alias, $mail, $address, $description, $logo, $currency = 'USD', $country = 'PA', $type = 'tiendita');
      \Drupal\tiendita_onboarding\Controller\TienditasController::assignTienditaPaymentGateways($tiendita->uuid(),$values['tiendita_payment_methods']);
      
     }
  
   }
 }